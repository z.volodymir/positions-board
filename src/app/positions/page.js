'use client';

import { useState } from 'react';
import dynamic from 'next/dynamic';
import { useLocalStorage } from '@/hooks/useLocalStorage';
import { defaultList } from '@/mock';
import Form from '@/components/Form';
const List = dynamic(() => import('@/components/List'), { ssr: false });

export default function Positions() {
  const [value, setValue] = useLocalStorage('list', defaultList);
  const [isShowForm, setIsShowForm] = useState(false);

  return (
    <div className="grid grid-cols-1 md:grid-cols-3 gap-y-4 md:gap-x-4 h-full">
      <div className="col-span-1">
        <List list={value} setList={setValue} setIsShowForm={setIsShowForm} />
      </div>
      {isShowForm && (
        <div className="col-span-2">
          <Form setList={setValue} setIsShowForm={setIsShowForm} />
        </div>
      )}
    </div>
  );
}

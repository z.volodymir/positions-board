import { v4 } from 'uuid';

export const defaultList = [
  {
    id: v4(),
    name: 'Новобранец',
    quantity: 10,
    price: 50,
    responsibilities: {},
  },
  {
    id: v4(),
    name: 'Рядовой',
    quantity: 5,
    price: 80,
    responsibilities: {},
  },
  {
    id: v4(),
    name: 'Сержант',
    quantity: 10,
    price: 100,
    responsibilities: {},
  },
];

export default function Content({ children }) {
  return (
    <div className="h-full md:h-content border border-t-0 border-gray rounded-content p-4 md:p-content">
      {children}
    </div>
  );
}

import React from 'react';

export default React.forwardRef(function Checkbox(
  { name, label, onBlur, onChange },
  ref
) {
  return (
    <div className="flex items-center gap-2 relative mt-2">
      <input
        type="checkbox"
        id={name}
        name={name}
        ref={ref}
        onBlur={onBlur}
        onChange={onChange}
        className="invisible w-checkbox h-checkbox"
      />
      <label htmlFor={name} className="label">
        {label}
      </label>
    </div>
  );
});

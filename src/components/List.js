'use client';

import { Reorder, AnimatePresence } from 'framer-motion';
import Item from './Item';

export default function List({ list, setList, setIsShowForm }) {
  return (
    <div className="flex flex-col justify-between gap-2 h-full">
      <div className="list-shaddow">
        <Reorder.Group
          axis="y"
          values={list}
          onReorder={setList}
          layoutScroll
          style={{ overflowY: 'scroll' }}
          className="grid gap-2 auto-rows-max md:h-[608px]"
        >
          <AnimatePresence initial={false}>
            {list?.length &&
              list.map((item) => {
                return <Item key={item.id} item={item} />;
              })}
          </AnimatePresence>
        </Reorder.Group>
      </div>

      <button
        className="w-full rounded shadow-button bg-violet p-button mb-4"
        onClick={() => setIsShowForm(true)}
      >
        Создать новую должность
      </button>
    </div>
  );
}

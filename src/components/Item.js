'use client';

import React from 'react';
import Image from 'next/image';
import { Reorder, useDragControls } from 'framer-motion';
import DotsIcon from 'public/assets/icons/dots.svg';

const variants = {
  initial: {
    opacity: 0,
    height: 0,
  },
  animate: {
    opacity: 1,
    height: '100%',
  },
  exit: {
    opacity: 0,
    height: 0,
  },
};

export default React.memo(
  function Item({ item }) {
    const controls = useDragControls();

    return (
      <Reorder.Item
        key={item.id}
        value={item}
        dragListener={false}
        dragControls={controls}
        whileDrag={{
          boxShadow: '4px 8px 40px 0px rgba(0, 0, 0, 0.40)',
        }}
        {...variants}
        className="flex gap-4 rounded bg-dark-light p-card select-none"
      >
        <div
          onPointerDown={(e) => controls.start(e)}
          className="flex justify-center items-center cursor-grab"
        >
          <Image src={DotsIcon} alt="" className="pointer-events-none" />
        </div>
        <div className="flex justify-between flex-grow">
          <div className="font-semi grid gap-[2px]">
            <h4>{item.name}</h4>
            <p className="text-small text-gray">{item.quantity} заданий</p>
          </div>
          <div className="text-small text-gray">
            <mark className="bg-transparent text-yellow font-bold">
              ${item.price}
            </mark>
            / час
          </div>
        </div>
      </Reorder.Item>
    );
  },
  (prevProps, nextProps) => {
    if (prevProps.item.id === nextProps.item.id) {
      return true;
    } else {
      return false;
    }
  }
);

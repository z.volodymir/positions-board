export default function Board({ children }) {
  return (
    <main className="min-h-screen flex flex-col items-center justify-center px-4 xl:px-0">
      <div className="w-full max-w-board my-board bg-dark p-board border border-gray rounded">
        {children}
      </div>
    </main>
  );
}

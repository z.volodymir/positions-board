'use client';

import { useForm } from 'react-hook-form';
import { v4 } from 'uuid';
import Checkbox from './Checkbox';

export default function Form({ setList, setIsShowForm }) {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    const { name, ...otherFields } = data;
    const responsibilitiesCheckedCount =
      Object.values(otherFields).filter(Boolean).length;

    const task = {
      id: v4(),
      name,
      quantity: responsibilitiesCheckedCount,
      price: responsibilitiesCheckedCount * 10,
      responsibilities: otherFields,
    };

    setList((list) => [task, ...list]);
    reset();
    setIsShowForm(false);
  };

  return (
    <div className="h-full rounded bg-dark-light p-4">
      <form
        className="grid grid-rows-[auto_1fr_auto] h-full"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="grid gap-[4px] font-semi rounded bg-dark p-form-group">
          <label htmlFor="name" className="text-small text-gray">
            Название
          </label>
          <input
            type="text"
            id="name"
            className="input"
            {...register('name', {
              required: true,
              pattern: /^[A-Za-z А-ЯЁ]+/i,
            })}
            aria-invalid={errors.name ? 'true' : 'false'}
          />
        </div>

        <div className="rounded bg-dark mt-[15px]">
          <p className="block text-left text-gray shadow-field rounded-tab p-[11px]">
            Обязаности
          </p>
          <div className="grid grid-cols-1 md:grid-cols-2 gap-[12px] p-[11px]">
            <div className="flex flex-col gap-3">
              <fieldset className="flex flex-col">
                <legend className="text-small text-gray">Торговля</legend>
                <Checkbox label="Продавать продукт" {...register('products')} />
                <Checkbox label="Выставлять цены" {...register('prices')} />
                <Checkbox
                  label="Смотреть аналитику"
                  {...register('analytics')}
                />
              </fieldset>
              <fieldset className="flex flex-col">
                <legend className="text-small text-gray">Разборки</legend>
                <Checkbox label="Дуель" {...register('duel')} />
                <Checkbox
                  label="Выставлять претензии"
                  {...register('claims')}
                />
              </fieldset>
            </div>
            <div className="flex flex-col gap-3">
              <fieldset className="flex flex-col">
                <legend className="text-small text-gray">Производство</legend>
                <Checkbox label="Закупать сырье" {...register('materials')} />
                <Checkbox label="Назначать рабочих" {...register('workers')} />
              </fieldset>
              <fieldset className="flex flex-col">
                <legend className="text-small text-gray">Управление</legend>
                <Checkbox
                  label="Назначать должности"
                  {...register('positions')}
                />
                <Checkbox label="Выгонять из банды" {...register('gang')} />
              </fieldset>
            </div>
          </div>
        </div>

        <button
          type="submit"
          className="w-full rounded shadow-button bg-violet p-button mt-2"
        >
          Сохранить
        </button>
      </form>
    </div>
  );
}

'use client';

import Link from 'next/link';
import { usePathname } from 'next/navigation';

const navLinks = [
  { label: 'Иерархия', href: '/hierarchy' },
  { label: 'Должности', href: '/positions' },
  { label: 'Список персонала', href: '/personnel' },
  { label: 'Наборы экипировки', href: '/equipments' },
];

export default function Navigation() {
  const pathName = usePathname();

  return (
    <nav>
      <ul className="grid grid-cols-1 md:grid-cols-4 items-center justify-between">
        {navLinks.map(({ label, href }) => {
          const isActive = pathName === href;

          return (
            <li
              key={href}
              className={`md:relative text-center border border-b-0 last:border-b first:rounded-tab border-gray md:border-b md:rounded-tab ${
                isActive
                  ? 'shadow-tab bg-dark border-l border-t border-b border-b-transparent md:z-[1] md:left-[-2px] md:w-[102%] md:first:left-0 md:first:w-[101%] md:last:w-[101%] md:h-[103%]'
                  : 'md:border-l-0 md:first:border-l'
              }`}
            >
              <Link
                href={href}
                className={`block py-tab ${
                  isActive ? 'text-violet' : 'text-gray-light'
                }`}
              >
                {label}
              </Link>
            </li>
          );
        })}
      </ul>
    </nav>
  );
}

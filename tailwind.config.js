/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ['var(--font-tt-smalls)'],
      },
      fontSize: {
        base: '14px',
        small: '12px',
      },
      fontWeight: {
        base: '500',
        semi: '600',
        bold: '700',
      },
      backgroundColor: {
        dark: '#292930',
        'dark-light': '#303038',
        violet: '#6764F1',
      },
      backgroundImage: {
        'card-shadow':
          'linear-gradient(180deg, rgba(41, 41, 48, 0.00) 0%, #292930 100%)',
      },
      colors: {
        white: '#F5F5F5',
        violet: '#6764F1',
        gray: '#6B6B7B',
        'gray-light': '#9C9CB0',
        yellow: '#EFB62B',
      },
      textShadow: {
        gray: ' 0px 4px 8px rgba(245, 245, 245, 0.24)',
      },
      boxShadow: {
        field: '0px 4px 16px 0px rgba(0, 0, 0, 0.24)',
        tab: '0px 4px 8px 0px rgba(103, 100, 241, 0.16) inset',
        button: '0px 4px 40px 0px rgba(104, 102, 234, 0.24)',
        card: '4px 8px 40px 0px rgba(0, 0, 0, 0.40)',
      },
      borderWidth: {
        DEFAULT: '2px',
      },
      borderColor: {
        dark: '#292930',
        gray: 'rgba(245, 245, 245, 0.08)',
        violet: '#6764F1',
      },
      borderRadius: {
        DEFAULT: '8px',
        tab: '8px 8px 0px 0px',
        content: '0px 0px 8px 8px;',
        checkbox: '4px',
      },
      outlineWidth: {
        DEFAULT: '2px',
      },
      padding: {
        board: '24px',
        card: '22px 16px 18px',
        content: '27px 40px 40px',
        tab: '12px',
        button: '14px',
        'form-group': '16px 16px 24px',
      },
      margin: {
        board: '122px',
      },
      maxWidth: {
        board: '1034px',
      },
      width: {
        checkbox: '24px',
      },
      height: {
        content: '747px',
        checkbox: '24px',
      },
    },
  },
  plugins: [],
};
